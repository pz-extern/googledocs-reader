<?php

namespace App\Model;

final class DotenvContainer
{
    /** @var string */
    private $documentId;
    /** @var string */
    private $credentialFile;
    /** @var string */
    private $tokenFile;

    public function __construct()
    {
        $this->documentId = $_ENV['document_id'] ? (string) $_ENV['document_id'] : '';
        $this->credentialFile = $_ENV['credential_file'] ? (string) $_ENV['credential_file'] : '';
        $this->tokenFile = $_ENV['token_file'] ? (string) $_ENV['token_file'] : '';
    }

    public function getDocumentId(): string
    {
        return $this->documentId;
    }

    public function getCredentialFile(): string
    {
        return $this->credentialFile;
    }

    public function getTokenFile(): string
    {
        return $this->tokenFile;
    }
}