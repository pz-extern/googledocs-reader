<?php

namespace App\Service;

use App\Model\DotenvContainer;
use Symfony\Component\Dotenv\Dotenv;

final class DotenvLoader
{
    /** @var string */
    private $envFile;
    /** @var DotenvContainer */
    private $environmentContainer;

    public function __construct(string $envFile)
    {
        $this->envFile = $envFile;

        $this->load();
        $this->environmentContainer = new DotenvContainer();
    }

    public function getEnvironment(): DotenvContainer
    {
        return $this->environmentContainer;
    }

    private function load(): void
    {
        $dotenv = new Dotenv();

        $dotenv->loadEnv($this->envFile);
    }
}