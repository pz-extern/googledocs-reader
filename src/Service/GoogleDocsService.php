<?php

namespace App\Service;

use App\Model\DotenvContainer;
use Google_Client;
use Google_Service_Sheets;

final class GoogleDocsService
{
    /** @var DotenvContainer */
    private $dotenvContainer;

    public function __construct(DotenvContainer $dotenvContainer)
    {
        $this->dotenvContainer = $dotenvContainer;
    }

    public function getSheetValues(string $cellRange = 'A1:C2'): array
    {
        $service = new Google_Service_Sheets(
            $this->getClient()
        );

        $response = $service->spreadsheets_values->get(
            $this->dotenvContainer->getDocumentId(),
            $cellRange
        );

        return $response->getValues();
    }

    private function getClient(): Google_Client
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
        $client->setAuthConfig($this->dotenvContainer->getCredentialFile());
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $tokenFile = $this->dotenvContainer->getTokenFile();

        if (file_exists($tokenFile)) {
            $accessToken = json_decode(file_get_contents($tokenFile), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenFile))) {
                mkdir(dirname($tokenFile), 0700, true);
            }
            file_put_contents($tokenFile, json_encode($client->getAccessToken()));
        }

        return $client;
    }
}