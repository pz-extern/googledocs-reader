<?php

include_once dirname(__DIR__) . '/vendor/autoload.php';

$envFile = dirname(__DIR__) . '/.env';
$environments = (new App\Service\DotenvLoader($envFile))->getEnvironment();

$values = (new App\Service\GoogleDocsService($environments))->getSheetValues();
if (empty($values)) {
    print "No data found.\n";
} else {
    foreach ($values as $row) {
        var_export($row);
    }
}